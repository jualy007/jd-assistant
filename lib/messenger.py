#!/usr/bin/env python
# -*- encoding=utf8 -*-

import datetime
import json
import smtplib
import traceback

import requests

from email.mime.text import MIMEText
from email.header import Header

from error.exception import SKException
from lib.log import logger


class WechatMessenger(object):
    """消息推送类"""

    def __init__(self, sc_key):
        if not sc_key:
            raise SKException("sc_key can not be empty")

        self.sc_key = sc_key

    def send(self, text, desp=""):
        if not text.strip():
            logger.error("Text of message is empty!")
            return

        now_time = str(datetime.datetime.now())
        desp = (
            "[{0}]".format(now_time) if not desp else "{0} [{1}]".format(desp, now_time)
        )

        try:
            resp = requests.get(
                "https://sc.ftqq.com/{}.send?text={}&desp={}".format(
                    self.sc_key, text, desp
                )
            )
            resp_json = json.loads(resp.text)
            if resp_json.get("errno") == 0:
                logger.info(
                    "Message sent successfully [text: %s, desp: %s]", text, desp
                )
            else:
                logger.error("Fail to send message, reason: %s", resp.text)
        except requests.exceptions.RequestException as req_error:
            logger.error("Request error: %s", req_error)
        except Exception as e:
            logger.error("Fail to send message [text: %s, desp: %s]: %s", text, desp, e)


class EmailMessenger(object):
    """消息推送类"""

    SMTP_SERVER = "smtp.qq.com"

    def __init__(self, addr, pwd):
        # 开启发信服务，这里使用的是加密传输
        self.email_server = smtplib.SMTP_SSL(host=self.smtp_server)
        self.email_server.connect(self.SMTP_SERVER, 465)
        # 登录发信邮箱
        self.email_server.login(addr, pwd)

        self.from_addr = addr
        self.password = pwd

    def send(self, to_addr, title, msgtext):
        try:
            # 邮箱正文内容，第一个参数为内容，第二个参数为格式(plain 为纯文本)，第三个参数为编码
            msg = MIMEText(msgtext, "plain", "utf-8")
            # 邮件头信息
            msg["From"] = Header(self.from_addr)
            msg["To"] = Header(to_addr)
            msg["Subject"] = Header(title, "utf-8")

            # 发送邮件
            self.email_server.sendmail(self.from_addr, to_addr, msg.as_string())
        except Exception as e:
            print(traceback.format_exc())
        finally:
            # 关闭服务器
            self.email_server.quit()
