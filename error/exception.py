#!/usr/bin/env python
# -*- encoding=utf8 -*-

from lib.log import logger


class SKException(Exception):
    def __init__(self, message):
        super().__init__(message)
        logger.error(message)
